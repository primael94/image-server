package fr.yoloc.image.service;

import java.awt.image.BufferedImage;
import java.io.File;

import fr.yoloc.image.domain.MediaImage;
import fr.yoloc.image.service.ImageService.DimensionList;

public interface StorageService {

	void write(BufferedImage image, DimensionList dimension, MediaImage mediaImage);
	
	File read(MediaImage mediaImage);	
}
