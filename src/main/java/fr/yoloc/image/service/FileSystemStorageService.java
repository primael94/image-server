package fr.yoloc.image.service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import fr.yoloc.image.domain.MediaImage;
import fr.yoloc.image.service.ImageService.DimensionList;

@ApplicationScoped
@Named("fileSystemStorageService")
public class FileSystemStorageService implements StorageService {

	@ConfigProperty(name="file.basePath")
	String basePath;
	
	@Override
	public void write(BufferedImage image, DimensionList dimension, MediaImage mediaImage) {
		String fileName = this.getFileName(dimension, mediaImage);
		try {
			ImageIO.write(image, mediaImage.getExtension(), new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public File read(MediaImage mediaImage) {
		Path path = Paths.get(basePath, mediaImage.getDimension().getSuffixe(), mediaImage.getBaseFileNameWithExtension());
		return path.toFile();
	}

	private String getFileName(DimensionList dimension, MediaImage mediaImage) {
		return String.join(File.separator, basePath, dimension.getSuffixe(), mediaImage.getBaseFileNameWithExtension());
	}

}
