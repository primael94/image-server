package fr.yoloc.image.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.inject.Named;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.common.collect.Lists;

import fr.yoloc.image.domain.MediaImage;
import fr.yoloc.image.service.ImageService.DimensionList;

@ApplicationScoped
@Named("googleStorageService")
public class GoogleStorageService implements StorageService {

	private Storage storage = null;

	@ConfigProperty(name = "google.bucket")
	String bucketName;

	@ConfigProperty(name = "google.credential")
	String credentialPath;

	private Storage authExplicit() {
		GoogleCredentials credentials = null;
		try {
			credentials = GoogleCredentials.fromStream(new FileInputStream(credentialPath))
					.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

		return storage;
	}

	public Storage getStorage() {
		if (this.storage == null) {
			this.storage = authExplicit();
		}

		return this.storage;
	}

	@Override
	public void write(BufferedImage image, DimensionList dimension, MediaImage mediaImage) {
		BlobId blobId = BlobId.of(bucketName, this.getFileName(dimension, mediaImage));
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("image/" + mediaImage.getExtension()).build();

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			ImageIO.write(image, mediaImage.getExtension(), os);
		} catch (IOException e) {
			e.printStackTrace();
		}

		this.getStorage().create(blobInfo, os.toByteArray());
	}

	@Override
	public File read(MediaImage mediaImage) {
		throw new RuntimeException("Appelez directement l'image du bucket");
	}

	private String getFileName(DimensionList dimension, MediaImage mediaImage) {
		return mediaImage.getBaseFileName() + DimensionList.separator + dimension.getSuffixe() + "."
				+ mediaImage.getExtension();
	}

}
