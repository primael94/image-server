package fr.yoloc.image.scaling;

public class Lanczos3Filter implements ResampleFilter {

	private final static float PI_FLOAT = (float) Math.PI;

	@Override
	public float getSamplingRadius() {
		return 3.0f;
	}

	@Override
	public float apply(float value) {
		if (value == 0) {
			return 1.0f;
		}
		if (value < 0.0f) {
			value = -value;
		}

		if (value < 3.0f) {
			value *= PI_FLOAT;
			return sincModified(value) * sincModified(value / 3.0f);
		} else {
			return 0.0f;
		}
	}

	@Override
	public String getName() {
		return "Lanczos3";
	}

	private float sincModified(float value) {
		return ((float) Math.sin(value)) / value;
	}

}
