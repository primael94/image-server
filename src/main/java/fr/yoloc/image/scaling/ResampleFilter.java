package fr.yoloc.image.scaling;

public interface ResampleFilter {

	float getSamplingRadius();

    float apply(float v);

	String getName();
	
}
