package fr.yoloc.image;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import fr.yoloc.image.service.ImageService;

@Path("/image")
public class ImageResource {

	@Inject
	ImageService imageService;

	@GET
	@Path("/{source}")
	public Response resample(@PathParam("source") String source) {
		System.out.println("resample " + source + ")");
		File image = imageService.getImage(source);

		if (image == null) {
			return Response.status(404).build();
		}

		String extension = FilenameUtils.getExtension(image.getName());

		return Response.status(200).header("Content-Type", "image/" + extension).entity(image).build();
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response postImage(MultipartFormDataInput input) {
		String fileName = "";

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("file");

		for (InputPart inputPart : inputParts) {

			try {

				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);

				// convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		return Response.status(201).entity("uploadFile is called, Uploaded file name : " + fileName).build();
	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}
}