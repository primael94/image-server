package fr.yoloc.image.service;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.yoloc.image.service.ImageService.DimensionList;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class ImageServiceTest {

	@Test
	public void test_construct_file_name() {
		
		Map<String, DimensionList> test_file_names = new HashMap<>();
		test_file_names.put("yoloc_md.jpeg", DimensionList.MD);
		test_file_names.put("yoloc_lg.jpeg", DimensionList.LG);
		test_file_names.put("yoloc_sm.jpeg", DimensionList.SM);
		test_file_names.put("yoloc_bucket.jpeg", DimensionList.ORIGINAL);
		test_file_names.put("yoloc.jpeg", DimensionList.ORIGINAL);
		test_file_names.put("yoloc_bucket_md.jpeg", DimensionList.MD);
		test_file_names.put("yoloc-md.jpeg", DimensionList.ORIGINAL);
		
		test_file_names.entrySet() //
		.forEach(e -> Assertions.assertEquals(e.getValue(), DimensionList.constructFileName(e.getKey())));
	}
	
}
