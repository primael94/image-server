package fr.yoloc.image.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.yoloc.image.service.ImageService.DimensionList;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class MediaImageTest {

	@Test
	public void test_md_image_file() {
		MediaImage media_test = new MediaImage("yoloc_md.jpeg");

		Assertions.assertEquals("yoloc_md", media_test.getFileNameWithoutExtension());
		Assertions.assertEquals("yoloc.jpeg", media_test.getBaseFileNameWithExtension());
		Assertions.assertEquals("yoloc_md.jpeg", media_test.getFileName());
		Assertions.assertEquals("yoloc", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.MD, media_test.getDimension());
	}

	@Test
	public void test_sm_image_file() {
		MediaImage media_test = new MediaImage("yoloc_sm.jpeg");

		Assertions.assertEquals("yoloc", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.SM, media_test.getDimension());
	}

	@Test
	public void test_lg_image_file() {
		MediaImage media_test = new MediaImage("yoloc_lg.jpeg");

		Assertions.assertEquals("yoloc", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.LG, media_test.getDimension());
	}

	@Test
	public void test_original_image_file() {
		MediaImage media_test = new MediaImage("yoloc.jpeg");

		Assertions.assertEquals("yoloc", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.ORIGINAL, media_test.getDimension());
	}

	@Test
	public void test_md_multiple_token_image_file() {
		MediaImage media_test = new MediaImage("yoloc_bucket_md.jpeg");

		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.MD, media_test.getDimension());
	}

	@Test
	public void test_sm_multiple_token_image_file() {
		MediaImage media_test = new MediaImage("yoloc_bucket_sm.jpeg");

		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.SM, media_test.getDimension());
	}

	@Test
	public void test_lg_multiple_token_image_file() {
		MediaImage media_test = new MediaImage("yoloc_bucket_lg.jpeg");

		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.LG, media_test.getDimension());
	}

	@Test
	public void test_original_multiple_token_image_file() {
		MediaImage media_test = new MediaImage("yoloc_bucket.jpeg");

		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.ORIGINAL, media_test.getDimension());
	}

	@Test
	public void test_original_to_sm() {
		MediaImage media_test = new MediaImage("yoloc_bucket.jpeg");

		Assertions.assertEquals("yoloc_bucket", media_test.getFileNameWithoutExtension());
		Assertions.assertEquals("yoloc_bucket.jpeg", media_test.getBaseFileNameWithExtension());
		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.ORIGINAL, media_test.getDimension());
		
		media_test.changeDimension(DimensionList.SM);
		
		Assertions.assertEquals("yoloc_bucket_sm", media_test.getFileNameWithoutExtension());
		Assertions.assertEquals("yoloc_bucket.jpeg", media_test.getBaseFileNameWithExtension());
		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.SM, media_test.getDimension());
	}
	
	@Test
	public void test_md_to_original() {
		MediaImage media_test = new MediaImage("yoloc_bucket_md.jpeg");

		Assertions.assertEquals("yoloc_bucket_md", media_test.getFileNameWithoutExtension());
		Assertions.assertEquals("yoloc_bucket.jpeg", media_test.getBaseFileNameWithExtension());
		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.MD, media_test.getDimension());
		
		media_test.changeDimension(DimensionList.ORIGINAL);
		
		Assertions.assertEquals("yoloc_bucket", media_test.getFileNameWithoutExtension());
		Assertions.assertEquals("yoloc_bucket.jpeg", media_test.getBaseFileNameWithExtension());
		Assertions.assertEquals("yoloc_bucket", media_test.getBaseFileName());
		Assertions.assertEquals("jpeg", media_test.getExtension());
		Assertions.assertEquals(DimensionList.ORIGINAL, media_test.getDimension());
	}
}
